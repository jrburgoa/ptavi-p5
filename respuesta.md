## Ejercicio 3. Análisis general
 
* ¿Cuántos paquetes componen la captura? 

    Hay 1050 Paquetes
* ¿Cuánto tiempo dura la captura?
    
    10.52s
* ¿Qué IP tiene la máquina donde se ha efectuado la captura?
    192.168.1.116
* ¿Se trata de una IP pública o de una IP privada?
* ¿Por qué lo sabes?
    Se trata de una IP privada, porque 192.168.0.0 está en el rango de IP's privadas

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
    UDP, RTP, SIP
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
    
    IPv4, ICMP
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
    EL protocolo RTP, el tráfico son 177332 bytes

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo corresponde con una llamada SIP.

Filtra por `sip` para conocer cuándo se envían paquetes SIP, o por 'rtp', para conocer cuándo se envían los RTP.

* ¿En qué segundos tienen lugar los dos primeros envíos SIP?
    En el segundo 0, ya se ven dos envios SIP
* Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
    En el paquete nº 6
* Los paquetes RTP, ¿cada cuánto se envían?
    Cada 0.01s

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).

Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 4. Primeras tramas
## Respuesta
Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y que funcionará por lo tanto como UA (user agent), y "Servidor" a la máquina que proporciona el servicio SIP. Todas las pregntas de este ejercicio se refieren a la captura cuando está filtrada de forma que sólo se ven tramoas del protocolo SIP.

* ¿De qué protocolo de nivel de aplicación son?
    Protocolo SIP
* ¿Cuál es la dirección IP de la máquina "Linphone"?
    Linhpone: 192.168.1.116
* ¿Cuál es la dirección IP de la máquina "Servidor"?
    Servidor: 212.79.111.155
* ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
    Linphone le envia un Request de Invitacion al Servidor
* ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
    Se ve como Linphone esta en un estado de 100Trying, 
es decir esta intentando comunicarse con el Servidor

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
    Del protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
    La primera(nº 4) del Servidor a Linhpone
    La segunda (nº 5) de Linphone al Servidor
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
    El servidor acepta el Request de Invitación que ha mandado Linphone en la primera trama
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
    Linphone responde con un ACK, es decir, responde al Servidor que ha recibido la respuesta que el propio servidor ha enviado

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 5. Tramas finales

Después de la trama 250, busca la primera trama SIP.

* ¿Qué número de trama es?
    La trama 1042
* ¿De qué máquina a qué máquina va?
    De Linhpone a Servidor
* ¿Para qué sirve?
    Es un Request BYE, para terminar la conexión
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
    SDP Version 0

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 6. Invitación

Seguimos centrados en los paquetes SIP. Veamos en particular el primer paquete (INVITE)

* ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
    sip:music@siptel.org
* ¿Qué instrucciones SIP entiende el UA?
*¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
    El cuerpo del mensaje
* ¿Cuál es el nombre de la sesión SIP?
    Se llama: "Talk"

## Ejercicio 7. Indicación de comienzo de conversación

En la propuesta SDP de Linphone puede verse un campo `m` con un valor que empieza por `audio 7078`.

* ¿Qué trama lleva esta propuesta?
    La trama Media Description
* ¿Qué indica el `7078`?
    El Media Port
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?

    Es el puerto que van a usar para comunicarse

* ¿Qué paquetes son esos?

  Los paquetes del Servidor a Linphone

* En la respuesta a esta propuesta vemos un campo `m` con un valor que empieza por `audio XXX`.
    Audio 29448
* ¿Qué trama lleva esta respuesta?
    La trama 4 en  Media description
* ¿Qué valor es el `XXX`?
    Es el Media Port del Servidor
* ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
    Es el puerto que usará Linphone para comunicarse con Servidor
* ¿Qué paquetes son esos?
    Paquetes de protocolo RTP de Linphone a Servidor

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el primer paquete RTP:

* ¿De qué máquina a qué máquina va?
    De Cliente a Servidor
* ¿Qué tipo de datos transporta?
    ITU-T G.711 PCMU
* ¿Qué tamaño tiene?
    En total RTP 172 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
    8 bits
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay?
· Deben haber 2 flujos
¿por qué?
·Porque es una llamada
* ¿Cuántos paquetes se pierden?
·No se pierde ningún paquete
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
·30.7267ms
* ¿Qué es lo que significa el valor de delta?
·Sería la latencia generada durante la transmision y el envio
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
·CUnado van del servidor hacia el cliente
* ¿Qué significan esos valores?
·El tiempo de retardo de la llegada de paquetes
Vamos a ver ahora los valores de una trama concreto, la númro 27. Vamos a analizarla en opción `Telephony`, `RTP`, `RTP Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
·El jitter son 7.11ms
·La Delta son 59.58ms
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
· Se puede saber con el "skew"
* El "skew" es negativo, ¿qué quiere decir eso?
· Que van a llegar paquetes mas tarde a lo previsto

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
· Una señal que será el inicio de audio
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
· Se oye una bajada de calidad
* ¿A qué se debe la diferencia?
· Que al bajar el tamaño del buffer, que era 50, ahora los paquetes que van llegando no se pueden almacenar
  y de esta manera se pueden descartar y/o perder

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` selecciona el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?
 10 segundos
Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se recibe el último OK que marca el final de la llamada?
· a los 10.52 segundos
  
Ahora, selecciona los dos streams, y pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
·Cliente: 0xD2DB8B4
 Servidor: 0x5C44A34B
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
· 514 paquetes
* ¿Cuál es la frecuencia de muestreo del audio?
· 8kHz
* ¿Qué formato se usa para los paquetes de audio (payload)?
· Formato: g711U

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 11. Captura de una llamada VoIP

Desde LinPhone, créate una cuenta SIP.  A continuación:

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 10 segundos de duración. Comienza a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Asegura (usando los filtros antes de guardarla) que en la captura haya solo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos RTP tiene esta captura?
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:

* Se valorará que haya realizado al menos haya ocho commits, correspondientes más o menos con los ejercicios pedidos, en al menos dos días diferentes, sobre la rama principal del repositorio.
* Se valorará que el fichero de respuestas esté en formato Markdown correcto.
* Se valorará que esté el texto de todas las preguntas en el fichero de respuestas, tal y como se indica al principio de este enunciado.
* Se valorará que las respuestas sean fáciles de entender, y estén correctamente relacionadas con las preguntas.
* Se valorará que la captura que se pide tenga exactamente lo que se pide.
* Se valorará que el fichero con el diagrama de la llamada VoIP tenga solo ese diagrama, en el formato que se indica en el enunciado.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para los archivos son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Cuando tengas la práctica lista, puedes realizar una prueba general, de que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Ejercicio 12 (segundo periodo). Llamada LinPhone

Ponte de acuerdo con algún compañero para realizar una llamada SIP conjunta (entre los dos) usando LinPhone, de unos 15 segundos de duración.  Si quieres, usa el foro de la asignatura, en el hilo correspondiente a este ejercicio, para enconrar con quién hacer la llamada.

Realiza una captura de esa llamada en la que estén todos los paquetes SIP y RTP (y sólo los paquetes SIP y RTP). No hace falta que tu compañero realice captura (pero puede realizarla si quiere que le sirva también para este ejercicio).

Guarda tu captura en el fichero `captura-a-dos.pcapng`

Como respuesta a este ejercicio, indica con quién has realizado la llamada, quién de los dos la ha iniciado, y cuantos paquetes hay en la captura que has realizado.

